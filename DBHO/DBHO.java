public class DBHO{
	public  static void main(String args[]) {
		
		//进制转换
		try{
			int In01 = Integer.parseInt(args[1]);//当前输入的数字的进制
			int Out01 = Integer.parseInt(args[2]);//需要输出的进制
			Translation tl1 = new Translation();
			System.out.println("Test~~~~~~~~"+In01+"进制转"+Out01+"进制~~~~~~~~ "+tl1.TL16to2(args[0],In01,Out01)+"~");
		} catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("请输入带转换文本：");
			System.out.println("待转文本 待转文本的进制 输出进制");
			System.out.println("eg: ff 16 2");
		}
		
	}
}

class Translation{
	public String TL16to2(String s,int IN,int OUT){//进制转换//16进制转二进制
		int t1 = Integer.parseInt(s,IN);//int t1 = Integer.parseInt(s,16);//当前输入的数字的进制
		String s1 = Integer.toString(t1,OUT);//String s1 = Integer.toString(t1,2);//需要输出的进制
		return s1;
	}
}

	/*测试程序
		String s1 = "10";
		
		int i1 = Integer.parseInt(s1);
		System.out.println("Test~~~~~~~~字符串转数值~~~~~~~~ "+i1+"~");
		int i2 = Integer.parseInt(s1,8);// parseInt("121",8)表示，双引号里面的字符121是个 8进制数，并不是说转化为8进制 
//		例如：int c=Integer.parseInt("12",8); 表示将可以理解为双引号里的12是个八进制的数，也就是二进制1010，转化为十进制就是10
		System.out.println("Test~~~~~~~~字符串转数值 8 进制~~~~~~~~ "+i2+"~");
		
		String s2 = Integer.toString(10,16);
		System.out.println("Test~~~~~~~~数值转字符串 8 进制~~~~~~~~ "+s2+"~"); */